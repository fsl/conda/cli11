This is a conda recipe for CLI11. This recipe is based on the
conda-forge/cli11-feedstock recipe, and incorporates some changes made by Rick
Lange for use with MMORF.

 - https://github.com/CLIUtils/CLI11
 - https://github.com/langefj/CLI11/commit/c92af3675641ebe5447992baab88b7b6a5d41564
 - https://github.com/conda-forge/cli11-feedstock
